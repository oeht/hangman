#-------------------------------------------------
#
# Project created by QtCreator 2015-07-08T10:16:28
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Hangman
TEMPLATE = app


SOURCES += main.cpp\
        frmmain.cpp \
    settings.cpp \
    sqlfunctions.cpp

HEADERS  += frmmain.h \
    settings.h \
    sqlfunctions.h

FORMS    += \
    frmmain.ui \
    settings.ui
