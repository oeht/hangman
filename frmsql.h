#ifndef FRMSQL_H
#define FRMSQL_H

#include <QWidget>

namespace Ui {
class frmSQL;
}

class frmSQL : public QWidget
{
    Q_OBJECT

public:
    explicit frmSQL(QWidget *parent = 0);
    ~frmSQL();

private:
    Ui::frmSQL *ui;
};

#endif // FRMSQL_H
