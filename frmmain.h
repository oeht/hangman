#ifndef FRMMAIN_H
#define FRMMAIN_H

#include "settings.h"
#include "sqlfunctions.h"

#include <QWidget>
#include <QDebug>
#include <QtSql/QtSql>
#include <QSignalMapper>
#include <QPushButton>
#include <QGraphicsScene>
#include <QSystemTrayIcon>

namespace Ui {
class frmMain;
}

class frmMain : public QWidget
{
    Q_OBJECT

public:
    explicit frmMain(QWidget *parent = 0);
    ~frmMain();

    void schreiben(QString buchstabe);
    void selectWort();
    void makeArrays(QString wort);
    void leiderVerloren();
    void checkForWin();
    void neuesWort();
    void buttonsShow(bool bShow);
    void buttonsHide();
    void bildAnzeigen(QString imagePath);



private slots:

    void slotFuermapperBuchstabeUebergeben(QWidget *pPushButton);

    void on_pbtNeu_clicked();

    void on_pbtSettings_clicked();

    void getGameLevel(int level);

private:
    Ui::frmMain *ui;

    static const int MAX = 20;
    static const int OFFSETX = 350;
    static const int OFFSETY = 50;
    int nWortSize;
    int nVersuche;
    QString sSelectedWort;
    QString sWort[MAX];
    QString sGeloest[MAX];
    QString sAusgabe;

    QSignalMapper   *mapperBuchstabeUebergeben;
    QPushButton     *pushButton;

    QPixmap image;
    QImage  *imageObject;
    QGraphicsScene *scene;

    Settings* settings;

    static const int AMOUNTBUTTONS = 26;
    QVector<QPushButton*> *abcButtons;
    QPushButton *btn;
    char abcChar;
    QString charToString;

    sqlfunctions *sqlfunct;

    //##### Game Level/Times #####

    int gameLevel;
    int timeLeter;
    int timeSum;

    QTimer *timerLetter;

    //############################
};

#endif // FRMMAIN_H
