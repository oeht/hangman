#ifndef SQLFUNCTIONS_H
#define SQLFUNCTIONS_H

#include <QString>
#include <QStringList>
#include <QtSql>
#include <QApplication>
#include <QDebug>

class sqlfunctions
{
private:
    QSqlDatabase hangmanDB;

    QString selectedWord;
    QStringList wordList;
    QString sqlBefehl;
    bool exists;

    const QString DB_FOLDER = QApplication::applicationDirPath() + "/DB";
    const QString DB_PATH = DB_FOLDER + "/hangmanDB.sqlite";

public:
    sqlfunctions(QString name); //open DB
    ~sqlfunctions(); //close DB

    void setConnectionName(QString name);

    QString sqlGetOneWord();
    QStringList sqlGetWordList();
    void sqlInsertWord(QString word);
    void sqlDeleteWord(QString word);
    bool sqlEntryExists(QString word);
};

#endif // SQLFUNCTIONS_H
