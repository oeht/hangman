#include "sqlfunctions.h"



sqlfunctions::sqlfunctions(QString name)
{

    hangmanDB = QSqlDatabase::addDatabase("QSQLITE", name);
    hangmanDB.setDatabaseName(DB_PATH);
    hangmanDB.open(); //constructor öffnet die verbindung

    if (!QDir(DB_FOLDER).exists())
    {
        QDir().mkdir(DB_FOLDER);
    }
}


sqlfunctions::~sqlfunctions()
{
    hangmanDB.close(); //destructor schließt die verbindung
}

// ein word von der db holen
QString sqlfunctions::sqlGetOneWord()
{
    QSqlQuery qry(hangmanDB);
    sqlBefehl = "SELECT wort FROM tblWoerter ORDER BY RANDOM() LIMIT 1;";
    qry.prepare(sqlBefehl);
    if(qry.exec()){
        if(qry.first()){
            selectedWord = qry.value(0).toString();
        }
        else qDebug() << "kein eintrag gefunden";
    }
    else qDebug() << "query nicht ausgeführt";
    return selectedWord;
}

//gesamte wordliste aus der db holen
QStringList sqlfunctions::sqlGetWordList()
{
    QSqlQuery qry(hangmanDB);
    sqlBefehl = "SELECT wort from tblWoerter ORDER BY wort ASC;";
    qry.prepare(sqlBefehl);
    if(qry.exec()){
        if(qry.first()){
            wordList.clear();
            do{
                wordList.append(qry.value(0).toString());
            }while(qry.next());
        }
        else qDebug() << "kein eintrag gefunden";
    }
    else qDebug() << "query nicht ausgeführt";
    return wordList;
}

//überprüfen ob ein word in der db existiert
bool sqlfunctions::sqlEntryExists(QString word)
{
    QSqlQuery qry(hangmanDB);
    sqlBefehl = "SELECT wort from tblWoerter where wort = :word ;";
    qry.prepare(sqlBefehl);
    qry.bindValue(":word", word);
    if(qry.exec()){
        if(qry.first()){
            exists = true;
        }
        else
        {
            exists = false;
        }
    }
    return exists;
}

//ein word in die db einfügen
void sqlfunctions::sqlInsertWord(QString word)
{
    QSqlQuery qry(hangmanDB);
    sqlBefehl = "INSERT INTO tblWoerter (wort) VALUES (:word);";
    qry.prepare(sqlBefehl);
    qry.bindValue(":word",word);
    if (!qry.exec())
    {
        qDebug() << "Insert nicht ausgeführt";
    }
}

//ein wort aus der sb löschen
void sqlfunctions::sqlDeleteWord(QString word)
{
    QSqlQuery qry(hangmanDB);
    sqlBefehl = "DELETE FROM tblWoerter where wort=:word;";
    qry.prepare(sqlBefehl);
    qry.bindValue(":word",word);
    if (!qry.exec())
    {
        qDebug() << "Delete nicht ausgeführt";
    }
}
