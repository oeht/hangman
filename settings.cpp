#include "settings.h"
#include "ui_settings.h"
#include <qDebug>

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

    sqlfunct = new sqlfunctions("settings");

    listeAktualisieren();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::listeAktualisieren()
{
    //Liste von Wörtern in ListWidget anzeigen
    ui->lwWortliste->clear();
    foreach (QString wort, sqlfunct->sqlGetWordList() )
    {
        ui->lwWortliste->addItem(wort);
    }

    ui->lwWortliste->scrollToTop();
}

void Settings::on_pbtInsert_clicked()
{
    if (!sqlfunct->sqlEntryExists(ui->edtWord->text()))
    {
        sqlfunct->sqlInsertWord(ui->edtWord->text());
        this->listeAktualisieren();
    }
    else qDebug() << "Eintrag existiert schon";

    ui->edtWord->clear();
}

void Settings::on_pbtDelete_clicked()
{
    sqlfunct->sqlDeleteWord(ui->lwWortliste->currentItem()->text());
    this->listeAktualisieren();
}

void Settings::on_spinBox_valueChanged(int arg1)
{
    emit setGameLevel(arg1);
}
