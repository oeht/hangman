#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "sqlfunctions.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

    void listeAktualisieren();

private slots:

    void on_pbtInsert_clicked();

    void on_pbtDelete_clicked();

    void on_spinBox_valueChanged(int arg1);

private:
    Ui::Settings *ui;

    sqlfunctions *sqlfunct;

signals:
    void setGameLevel(int);
};

#endif // SETTINGS_H
