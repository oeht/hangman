#include "frmmain.h"
#include "ui_frmmain.h"
#include <QPushButton>
#include <QDebug>
#include <QDialog>

frmMain::frmMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::frmMain)
{
    ui->setupUi(this);

    //Instanzierungen
    settings                    = new Settings;
    sqlfunct                    = new sqlfunctions("frmmain");
    mapperBuchstabeUebergeben   = new QSignalMapper;
    imageObject                 = new QImage;
    scene                       = new QGraphicsScene;
    abcButtons                  = new QVector<QPushButton*>;
    timerLetter                 = new QTimer;

    //Connections
    connect(settings,       SIGNAL(setGameLevel(int)),
            this,           SLOT(getGameLevel(int))
            );

    //Buttons erstellen, mappen und in das QVector einfügen
    for ( int i = 0; i < AMOUNTBUTTONS; i++)
    {
        QPushButton* btn = new QPushButton(this);
        btn->resize(30,30);
        btn->move( i*40 - (int(i/6)*6*40) + OFFSETX, (int(i/6)*40) + OFFSETY );

        abcChar = char(65+i);
        charToString.clear();
        charToString.append(abcChar);
        btn->setText(charToString);
        btn->setStyleSheet("background-color:#d1c7b9;");

        mapperBuchstabeUebergeben->setMapping(btn, btn);

        connect(btn,                        SIGNAL(clicked()),
                mapperBuchstabeUebergeben,  SLOT(map())
                );

        abcButtons->append(btn);
    }

    connect(mapperBuchstabeUebergeben,  SIGNAL(mapped(QWidget*)),
            this,                       SLOT(slotFuermapperBuchstabeUebergeben(QWidget*))
            );

    //Alle Buchstaben erstmal abschalten bis das Spiel beginnt
    buttonsShow(false);
}


// Destruktor
frmMain::~frmMain()
{
    delete ui;
}


// Buchstabend schreiben und Buchstabe-Button deaktivieren
void frmMain::slotFuermapperBuchstabeUebergeben(QWidget* pPbtWidget)
{
    btn = qobject_cast<QPushButton *>(pPbtWidget);
    schreiben(btn->text());
    btn->setDisabled(true);
}


// Je nach übrigen Versuchen das richtige Bild anzeigen
void frmMain::bildAnzeigen(QString imagePath)
{
    imageObject->load(imagePath);
    image = QPixmap::fromImage(*imageObject);
    scene->addPixmap(image);
    scene->setSceneRect(image.rect());
    ui->myGraphic->setScene(scene);
}

// Setzt die Versuche wieder auf 10 und ruft die Methode auf eine neues Wort aus der Datenbank zu laden
void frmMain::neuesWort()
{
    nVersuche= 10;
    bildAnzeigen("bilder\\blank.jpg");
    selectWort();
    ui->lblVersuche->setText("Versuche übrig:  " + QString::number(nVersuche));
}


// Selektiert ein neues Wort aus der Datenbank und ruft die Methode auf, die Arrays aufzubauen
void frmMain::selectWort()
{
    makeArrays(sqlfunct->sqlGetOneWord());
}


// Baut die Arrays auf und setzt den Ausgabetext
void frmMain::makeArrays(QString wort)
{
    nWortSize = wort.size();

    for (int i=0; i < nWortSize; i++ )
    {
        sGeloest[i] = "-";
        sWort[i]    = wort.mid(i,1);
        sAusgabe    = sAusgabe + "- ";
    }

    ui->lblAusgabe->setText(sAusgabe);
}



// Hauptmethode zum suchen von Buchstaben in den Arrays
void frmMain::schreiben(QString buchstabe)
{

    bool bBuchstabeErraten;
    sAusgabe.clear();

    for (int i=0; i < nWortSize; i++ )
    {
        if ( sWort[i] == buchstabe )
        {
            sGeloest[i] = sWort[i];
            bBuchstabeErraten = true;
        }//if
        sAusgabe.append(sGeloest[i]).append(" ");
    }//for

    if ( !bBuchstabeErraten )
    {
        nVersuche--;
        if ( nVersuche > 0 )
        {
            ui->lblVersuche->setText("Versuche übrig:  " + QString::number(nVersuche));
            bildAnzeigen("bilder\\hangman_" + QString::number(nVersuche) + ".jpg");
        }//if
        else
        {
            leiderVerloren();
            bildAnzeigen("bilder\\hangman_0.jpg");
        }//else

    }//if
    else
    {
        checkForWin();
    }//else

    ui->lblAusgabe->setText(sAusgabe);
    sAusgabe.clear();
}


// Methode zur Überprüfung ob man Gewonnen hat
// Es wird einfach nur überprüft ob ein "-" noch im Array existiert
void frmMain::checkForWin()
{

    if ( !sAusgabe.contains("-") )
    {
        ui->lblVersuche->setText("Gewonnen");
        ui->pbtNeu->setDisabled(false);
        buttonsShow(false);
    }
}

//Diese Methode wird aufgerufen wenn alle Versuche verbraucht sind
//Sie enabled/disabled auch die Buttons
void frmMain::leiderVerloren()
{
    ui->lblVersuche->setText("Leider Verloren");
    sAusgabe.clear();

    for (int i=0; i < nWortSize; i++ )
    {
        sAusgabe.append(sWort[i]).append(" ");
    }

    ui->lblAusgabe->setText(sAusgabe);
    ui->pbtNeu->setDisabled(false);
    buttonsShow(false);
}

//Button NEU CLICKED SLOT
void frmMain::on_pbtNeu_clicked()
{
    ui->pbtNeu->setDisabled(true);
    neuesWort();
    buttonsShow(true);
}


//Methode zum enablen/disablen aller Buchstaben-Knöpfe
void frmMain::buttonsShow(bool bShow)
{
    for( int i=0; i < AMOUNTBUTTONS; i++)
    {
        btn = abcButtons->at(i);
        btn->setEnabled(bShow);
    }
}

//Settings Dialog anzeigen
void frmMain::on_pbtSettings_clicked()
{
    settings->show();
}


void frmMain::getGameLevel(int level)
{
    this->gameLevel = level;
}
